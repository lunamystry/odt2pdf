// @flow
import chai, {assert} from 'chai'
import chaiString from 'chai-string'
import path from 'path'
import * as fse from 'fs-extra'

import * as odt2pdf from './odt2pdf'

chai.use(chaiString);

describe('odt2pdf', () => {
  it('can get xml of content.xml', async () => {
    const testOdt = path.join(__dirname, 'testData', 'test.odt');
    const contentXml = await odt2pdf.getContent(testOdt);
    assert.isDefined(contentXml)
  })

  it('can get xml of content.xml', async () => {
    const data = {
      justAField: 'just a field',
      dontEscape: "<svg>don't escape</svg>",
      showBlock: true
    };

    const input = `
<!doctype html>
<html>
  <head>
  </head>
  <body>
    {{justAField}}
    {{{dontEscape}}}
    {{#if showBlock }}
      This is a block that is shown
    {{/if}}
  </body>
</html>`;

    const expected = `
<!doctype html>
<html>
  <head>
  </head>
  <body>
    just a field
    <svg>don't escape</svg>
      This is a block that is shown
  </body>
</html>`;

    const template = odt2pdf.renderXML(data, input);
    assert.equalIgnoreSpaces(template, expected);
  })


  it('can strip XML between handlebars ', () => {
    const unsanitised = `
      <text:p text:style-name="Preformatted_20_Text">{{/if}}</text:p>
      <text:p text:style-name="P1"/>
      <text:p text:style-name="P6">{{#unless <text:span text:style-name="T2">showBlock</text:span>}}</text:p>
      <text:p text:style-name="P2">
        <text:s text:c="5"/>
      </text:p>
      <table:table table:name="Table1" table:style-name="Table1">
        <table:table-column table:style-name="Table1.A"/>
        <table:table-column table:style-name="Table1.B"/>
        <table:table-row>
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P5">This is a hidden table</text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.B1" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
        </table:table-row>
        <table:table-row>
          <table:table-cell table:style-name="Table1.A2" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.B2" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
        </table:table-row>
      </table:table>
      <text:p text:style-name="P2"/>
      <text:p text:style-name="Preformatted_20_Text">{{/unless}}</text:p>
      {{#if <text:span> something <span:s/>}}
    `
    const expected = `
      <text:p text:style-name="Preformatted_20_Text">{{/if}}</text:p>
      <text:p text:style-name="P1"/>
      <text:p text:style-name="P6">{{#unless showBlock}}</text:p>
      <text:p text:style-name="P2">
        <text:s text:c="5"/>
      </text:p>
      <table:table table:name="Table1" table:style-name="Table1">
        <table:table-column table:style-name="Table1.A"/>
        <table:table-column table:style-name="Table1.B"/>
        <table:table-row>
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P5">This is a hidden table</text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.B1" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
        </table:table-row>
        <table:table-row>
          <table:table-cell table:style-name="Table1.A2" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.B2" office:value-type="string">
            <text:p text:style-name="Table_20_Contents"/>
          </table:table-cell>
        </table:table-row>
      </table:table>
      <text:p text:style-name="P2"/>
      <text:p text:style-name="Preformatted_20_Text">{{/unless}}</text:p>
    {{#if something }}
    `

    const result = odt2pdf.stripXMLBetweenHandlebars(unsanitised);
    assert.equalIgnoreSpaces(result, expected)
  })

  it('can do give back an odt (Manual test)', async () => {
    const testOdt = path.join(__dirname, 'testData', 'basic.odt');
    const data = {
      justAField: 'just a field',
      dontEscape: "<svg>don't escape</svg>",
      showBlock: true
    };

    const input = await odt2pdf.getContent(testOdt)
    const sanitizedInput = odt2pdf.stripXMLBetweenHandlebars(input)
    const rendered = odt2pdf.renderXML(data, sanitizedInput);
    const zip = await odt2pdf.setContentOnTemplate(testOdt, rendered);
    await odt2pdf.saveOdt('out.odt', zip)
  })

  it('can do give back an odt (Manual test)', async () => {
    const testOdt = path.join(__dirname, 'testData', 'basic.odt');
    const data = {
      justAField: 'just a field',
      dontEscape: "<svg>don't escape</svg>",
      showBlock: true
    };

    await odt2pdf.default(testOdt, 'piped.odt', data)
  })
});
