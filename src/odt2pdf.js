// @flow
import * as JSZip from 'jszip'
import * as R from 'ramda'
import * as fse from 'fs-extra'
import * as handlebars from 'handlebars'
import path from 'path'

export const dontExist = async (paths:string []): Promise<string []> => {
  const doesPathExist = async (path:string) => {
    const exists = await fse.pathExists(path);
    return { exists, path };
  };

  const withExistsStatus = await Promise.all(paths.map(doesPathExist));

  return withExistsStatus
    .filter(p => !p.exists)
    .map(p => p.path);
};

const zip = async (odtPath:string) => {
  const nonExistantRequiredPaths = await dontExist([odtPath]);
  if (nonExistantRequiredPaths.length > 0) {
    const errorMsg = `required path not found: "${nonExistantRequiredPaths.join(', ')}"`;
    throw new Error(errorMsg)
  }

  // read a zip file
  const fileStream = await fse.readFile(odtPath);
  return JSZip.loadAsync(fileStream);
}

export const getContent = async (odtPath:string) => (await zip(odtPath)).file('content.xml').async("string")

export const stripXMLBetweenHandlebars = (content:string) => content.replace(/({{.*?}})/gm, (a:string) => a.replace(/<[^>]+>/gm, ""))

export const renderXML = R.curry((data:any, str: string):string => handlebars.compile(str)(data));

export const setContentOnTemplate = R.curry(async (odtPath:string, newContent:string) => (await zip(odtPath)).file('content.xml', newContent))

export const saveOdt = R.curry((odtName:string, zip: any) => new Promise((resolve, reject) =>
  zip
    .generateNodeStream({streamFiles:true})
    .pipe(fse.createWriteStream(odtName))
    .on('finish', resolve)
    .on('error', reject)
))

export default (templatePath:string, outputPath:string, data:any) => R.pipe(
  getContent,
  R.then(stripXMLBetweenHandlebars),
  R.then(renderXML(data)),
  R.then(setContentOnTemplate(templatePath)),
  R.then(saveOdt(outputPath))
)(templatePath)
